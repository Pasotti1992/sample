package com.example.matteopasotti.sampleapplication.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import com.example.matteopasotti.sampleapplication.R;
import com.example.matteopasotti.sampleapplication.databinding.ActivityMainBinding;
import com.example.matteopasotti.sampleapplication.factory.AppViewModelFactory;
import dagger.android.AndroidInjection;


import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {


    private ActivityMainBinding binding;

    @Inject
    AppViewModelFactory appViewModelFactory;

    private MainActivityViewModel mainActivityViewModel;

    private final Observer<String> resultObserver = new Observer<String>() {
        @Override
        public void onChanged(String s) {
            Toast.makeText(MainActivity.this, s, Toast.LENGTH_LONG).show();
        }

    };

    private final Observer<Integer> progressObserver = new Observer<Integer>() {
        @Override
        public void onChanged(Integer s) {
            Toast.makeText(MainActivity.this, s, Toast.LENGTH_LONG).show();
        }

    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this , R.layout.activity_main);

        mainActivityViewModel = ViewModelProviders.of(this, appViewModelFactory).get(MainActivityViewModel.class);
        binding.setViewModel(mainActivityViewModel);

        binding.startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivityViewModel.toggleTimer();
            }
        });

        this.mainActivityViewModel.resultLiveData.observe(this, this.resultObserver);
        this.mainActivityViewModel.progressLiveData.observe(this, this.progressObserver);


        mainActivityViewModel.runAsyncTaskCose();
        mainActivityViewModel.runAsyncTaskCose();
        mainActivityViewModel.runAsyncTaskCose();
        mainActivityViewModel.runAsyncTaskCose();
        mainActivityViewModel.runAsyncTaskCose();
        mainActivityViewModel.runAsyncTaskCose();
        mainActivityViewModel.runAsyncTaskCose();
    }
}
