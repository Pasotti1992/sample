package com.example.matteopasotti.sampleapplication;

import android.app.Activity;
import android.app.Application;
import com.example.matteopasotti.sampleapplication.di.AppComponent;
import com.example.matteopasotti.sampleapplication.di.DaggerAppComponent;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

import javax.inject.Inject;

public class MyApplication extends Application implements HasActivityInjector {

    @Inject
    DispatchingAndroidInjector<Activity> activityInjector;

    AppComponent appComponent = DaggerAppComponent.builder()
            .application(this)
            .build();

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent.inject(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityInjector;
    }
}
