package com.example.matteopasotti.sampleapplication.di;

import com.example.matteopasotti.sampleapplication.view.MainActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {


    @ContributesAndroidInjector
    abstract MainActivity contributeMainActivity();
}
