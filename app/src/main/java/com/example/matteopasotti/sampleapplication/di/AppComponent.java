package com.example.matteopasotti.sampleapplication.di;


import android.app.Application;
import com.example.matteopasotti.sampleapplication.MyApplication;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

import javax.inject.Singleton;

@Singleton
@Component(modules = {
        NetModule.class,
        ActivityModule.class,
        ViewModelModule.class,
        AndroidSupportInjectionModule.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        AppComponent.Builder application(Application application);

        AppComponent build();
    }

    void inject(MyApplication myApplication);
}
