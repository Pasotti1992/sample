package com.example.matteopasotti.sampleapplication.view;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;
import androidx.databinding.Bindable;
import androidx.databinding.Observable;
import androidx.databinding.ObservableField;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.matteopasotti.sampleapplication.repository.MainRepository;
import io.reactivex.*;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import javax.inject.Inject;
import java.sql.Time;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;

public class MainActivityViewModel extends ViewModel {

    private MainRepository mainRepository;

    public ObservableField<String> text;
    public int currentTimeInterval; // in seconds

    private Timer mTimer;


    public MutableLiveData<Integer> progressLiveData = new MutableLiveData<>();

    public MutableLiveData<String> resultLiveData = new MutableLiveData<>();

    @Inject
    public MainActivityViewModel(MainRepository mainRepository) {
        this.text = new ObservableField<String>("00:00");
        this.currentTimeInterval = 0;
        this.mainRepository = mainRepository;
    }

    public void toggleTimer() {
        if (mTimer == null) {
            this.startTimer();
        } else {
            this.stopTimer();
        }
    }

    public void startTimer() {
        if (mTimer != null) {
            stopTimer();
        }

        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                currentTimeInterval += 1;
                setTime();
            }
        }, 0, 1*1000);
    }

    public void stopTimer() {
        if (mTimer == null) {
            return;
        }

        mTimer.cancel();
        mTimer = null;
    }

    private void setTime() {
        int numberOfMinutes = this.currentTimeInterval / 60;
        int numberOfSeconds = this.currentTimeInterval % 60;

        String minuteString = numberOfMinutes < 10 ? "0" + String.valueOf(numberOfMinutes) : String.valueOf(numberOfMinutes);
        String secondString = numberOfSeconds < 10 ? "0" + String.valueOf(numberOfSeconds) : String.valueOf(numberOfSeconds);

        this.text.set(minuteString + ":" + secondString);
    }

    public void runAsyncTaskCose() {

        @SuppressLint("StaticFieldLeak") AsyncTask<String, Integer, String> asyncTask = new AsyncTask<String, Integer, String>() {
            @Override
            protected String doInBackground(String... strings) {
                // strings == "input"
                int i = 1;
                while (i < 5) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    i++;
                    publishProgress(i);
                }

                return "ciao";
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);


                MainActivityViewModel.this.progressLiveData.setValue(values[0]);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                // s == "ciao"
                MainActivityViewModel.this.resultLiveData.setValue(s);
            }
        };
        asyncTask.execute("input");
    }

    public void runRXJavaCose() {

        io.reactivex.Observable observable = io.reactivex.Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(ObservableEmitter<Integer> emitter) throws Exception {

                int i = 1;
                while (i < 5) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    i++;
                    emitter.onNext(i);
                }
                emitter.onComplete();

            }
        });


        observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Integer>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Integer progress) {
                        MainActivityViewModel.this.progressLiveData.setValue(progress);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        MainActivityViewModel.this.resultLiveData.setValue("ciao");
                    }
                });

    }
}
